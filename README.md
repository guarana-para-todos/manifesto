# Projeto

__Nome Projeto__ : `Guaraná Para Todos (GPT)`

__Dominio__: [gpt.ong.br](https://gpt.ong.br)

__Autores__ :

- `Jefferson Lisboa <lisboa.jeff@gmail.com>`

__Objetivo__: `Facilitar comunicação entre doadores e pessoas carentes`

---

## Manifesto

`Tirem os olhos do curumim e plantem-no na terra firme, reguem-no com lágrimas durante 4 luas e ali nascerá a "planta da vida", ela dará força aos jovens e revigorará os velhos`

Na lenda indigena¹, `guaraná` representa a planta da vida, vida essa que precisa ser preservada.

De acordo com os últimos dados divulgados², o Brasil voltou ao `mapa` da fome e apresenta um quadro sério de `pobreza menstrual`³.

O projeto `GPT` tem como objetivo facilitar a `interação`, entre `pessoas` ou `empresas` doadoras, com `instituições` de apoio social. Estás tem como destino encaminhar doações as pessoas `carentes` (beneficiados) que participam de programas `sociais`.

---

## Componentes

- Beneficiados
- Instituições sociais
- Doadores

## Ambiente

- Cidades do território Brasileiro

---

## Atores

| Identificação | Descrição          |
| ------------- | ------------------ |
| BF            | Beneficiado        |
| IDAS          | Instituição Social |
| DR            | Doador             |

## Atividades

| Identificação | Descrição                       | Objetivo                                                         |
| ------------- | ------------------------------- | ---------------------------------------------------------------- |
| A00           | Participar do Programa          | Viabilizar o cadastro de participantes do programa               |
| A01           | Listar Itens Doáveis            | Apresentar informações de itens doáveis                          |
| A02           | Requisitar Doação               | Requisitar doações para instituições e beneficiados              |
| A03           | Visualizar Realizações          | Visualizar informações de doações realizadas                     |
| A04           | Realizar Doação                 | Concluir doação de itens a uma instituição selecionada           |
| A05           | Consultar Situação Doação       | Doadores e instituições podem consultar situação de doação       |
| A06           | Visualizar Requisições          | Visualizar informações de doações requisitadas                   |
| A07           | Consultar Benefício             | Facilitar a consulta de benefício concedido                      |
| A08           | Gerenciar Beneficiados          | Facilitar o gerenciamento de beneficiados em uma instituição     |
| A09           | Analisar Situação Beneficiado   | Verifica situação cadastral de beneficiados                      |
| A10           | Analisar Situação Institucional | Verifica situação cadastral de instituições                      |
| A11           | Gerenciar itens doáveis         | Gerenciar informações de itens doáveis                           |
| A12           | Pesquisar Instituição           | Pesquisar localização de uma instituição de apoio social próxima |

---

Abaixo temos disponível diagramas de caso de uso do ecossistema GPT

## Casos de Uso - Primários

```plantuml
@startuml

actor "Beneficiado" as BF
actor "Instituição Social"  as IDAS
actor "Doador" as DR

usecase "Consultar Situação Doação" as A05
usecase "Listar Itens Doáveis" as A01


package Doadores {

    usecase "Visualizar Realizações" as A03
    DR --> A03
    A03<.. A05: <<extends>>
    
    usecase "Realizar Doação" as A04
    DR -right-> A04

    usecase "Pesquisar Instituição" as A12
    A04 <-right.. A12 : <<extends>>
}


note top of (A04)
    Para completar a etapa de doação,
    deve ser informado o código de doação
    estabelecido pela instituição de apoio social
end note

note right of (A12)
    Deve informar itens com maior 
    urgência a doação
end note


package Social {

    usecase "Requisitar Doação" as A02
    A02 <-left-- IDAS
    A02 ..|> A01 : <<include>>
    
    usecase "Consultar Benefício" as A07
    BF --> A07

    usecase "Gerenciar Beneficiados" as A08
    IDAS --> A08
    A07 -left..> A08: <<extends>>

    usecase "Visualizar Requisições" as A06
    IDAS --> A06
    A06 <.. A05 : <<extends>>
    
}

note top of (BF)
  Caso o beneficiado tenha condições,
  será possível que faça o próprio 
  cadastro no GPT
end note


```

---

Abaixo listamos os casos de uso de gerenciamento administrativo do sistema

## Caso de Uso - Secundários

```plantuml

@startuml

actor "Gerenciador Social" <<Sistema>>  as GS
actor "Administrador" <<Funcionário>> as AF

package sistema {

  usecase "Analisar Situação Beneficiado" as A09
  GS --> A09

  usecase "Analisar Situação Institucional" as A10
  GS --> A10


  usecase "Gerenciar itens doáveis" as A11
  AF --> A11

}


```

---

## Referências

1 - `https://pt.wikipedia.org/wiki/Guaraná`

2 - `https://jornalistaslivres.org/de-um-lado-esse-carnaval-de-outro-a-fome-total`

3 - `https://www12.senado.leg.br/noticias/infomaterias/2021/07/o-que-e-pobreza-menstrual-e-por-que-ela-afasta-estudantes-das-escolas`
